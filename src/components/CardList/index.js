import React, { Component } from 'react';
import Card from '../Card';
import { Input } from 'antd';

import s from './CardList.module.scss';
import getTranslateWord from "../services/yandex-dictionary";

const { Search } = Input;

class CardList extends Component {
    state = {
        value:'',
        label: '',
        isBusy: false
    }
    handleInputChange = (e) => {
        this.setState({
            value: e.target.value
        });
    }

    getWord = async () => {
        const { value } = this.state;
        const getWord = await getTranslateWord(value);
        const translateWord = getWord[0].tr[0].text;
        this.setState(({value}) => {
            return {
                label: `${value} - ${translateWord}`,
                value: '',
                isBusy: false,
            }
        })
    }

    handleSubmitForm = async () => {
        this.setState({
            isBusy: true,
            }, this.getWord);
    }
  render() {
      const { item = [], onDeletedItem } = this.props;
      const { value, label, isBusy } = this.state;
      return (
          <>
              <div>
                  { label }
              </div>
              <div className={s.form}>
                  <Search
                      placeholder="input search text"
                      enterButton="Search"
                      size="large"
                      value={value}
                      loading={isBusy}
                      onChange={this.handleInputChange}
                      onSearch={this.handleSubmitForm}
                  />
              </div>

              <div className={s.root}>
                  {
                      item.map(({ eng, rus, id}) => (
                          <Card
                              onDeleted={ () => {
                                  onDeletedItem(id);
                              }}
                              key={id}
                              eng={eng}
                              rus={rus}
                          />
                      ))
                  }
              </div>
          </>
      );
  }
}

export default CardList;
