import React, { Component } from 'react';
import HeaderBlock from './components/HeaderBlock'
import Header from "./components/Header";
import Paragraph from "./components/Paragraph";
import CardList from "./components/CardList";
import wordsList from "./components/wordList";


class App extends Component {
    state = {
        wordArr: wordsList,
    }

    handleDeletedItem = (id) => {
        this.setState(({wordArr}) => {
            const idx = wordArr.findIndex(item => item.id === id);

            const newWordArr = [
                ...wordArr.slice(0, idx),
                ...wordArr.slice(idx + 1)
                ];

            return {
                wordArr: newWordArr,
            };
        });
    }

        render() {
        const { wordArr } = this.state;
        return (
            <>
                <HeaderBlock>
                    <Header>
                        Время учить слова онлайн
                    </Header>
                    <Paragraph>
                        Воспользуйтесь карточками для запоминания и пополняйте свой активный словарный запас.
                    </Paragraph>
                </HeaderBlock>
                <CardList onDeletedItem={this.handleDeletedItem}
                          item={wordArr}
                />
                <HeaderBlock hideBackground>
                    <Header>
                        Let's starting!
                    </Header>
                    <Paragraph>
                        Добавляйте новые слова и запоминайте их легко и быстро, просто кликая по карточкам.
                    </Paragraph>
                </HeaderBlock>
            </>
        );
    }

}

export default App;
